# Segment Tree - Learning and Testing

I created this repository to deepen my understanding of Segment Trees, by
creating an implementation and conducting tests. I've been transitioning towards 
LC Mediums to Hard, and some puzzles require knowledge of Segment Trees.