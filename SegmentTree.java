public class SegmentTree {

    // For any range denoted a to b, the range is a to b inclusive: [a,b]
    class SegmentTreeNode {
        int start;
        int end;
        int sum;
        SegmentTreeNode left;
        SegmentTreeNode right;
        public SegmentTreeNode(int start, int end) {
            this.start = start;
            this.end = end;
            this.sum = 0;
            this.left = null;
            this.right = null;
        }
    }

    public SegmentTreeNode root;

    public SegmentTree(int[] nums) {
        root = buildTree(nums, 0, nums.length - 1);
    }

    public SegmentTreeNode buildTree(int[] nums, int start, int end) {
        if (start > end) return null;
        SegmentTreeNode node = new SegmentTreeNode(start, end);
        // Leaf Node
        if (start == end) {
            node.sum = nums[start];
        } else {
            int mid = start + (end - start) / 2;
            node.left = buildTree(nums, start, mid);
            node.right = buildTree(nums, mid + 1, end);
            node.sum = node.left.sum + node.right.sum;
        }
        return node;
    }

    // Query the range: [a,b]
    public int rangeSum(int a, int b) {
        return rangeSum(root, a, b);
    }

    private int rangeSum(SegmentTreeNode node, int start, int end) {
        if (node == null || start > node.end || end < node.start) return 0;
        //If the node is entirely within the query range
        if (start <= node.start && end >= node.end) {
            return node.sum;
        }
        return rangeSum(node.left, start, end) + rangeSum(node.right, start, end);
    }

    // Update wrt to the original array: index i, to val
    public void update(int i, int val) {
        update(root, i, val);
    }

    private void update(SegmentTreeNode node, int index, int val) {
        // Reached the leaf node
        if (node.start == node.end) {
            node.sum = val;
        } else {
            int mid = node.start + (node.end - node.start) / 2;
            if (index <= mid) {
                update(node.left, index, val);
            } else {
                update(node.right, index, val);
            }
            node.sum = node.left.sum + node.right.sum;
        }
    }




}
