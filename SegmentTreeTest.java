import org.junit.jupiter.api.*;


class SegmentTreeTest {

    @Test
    void testSingleElementArray() {
        SegmentTree st = new SegmentTree(new int[]{5});
        Assertions.assertEquals(5, st.rangeSum(0, 0));
        st.update(0, 10);
        Assertions.assertEquals(10, st.rangeSum(0, 0));
    }

    @Test
    void testSimpleRangeSum() {
        SegmentTree st = new SegmentTree(new int[]{1, 2, 3, 4});
        Assertions.assertEquals(6, st.rangeSum(0, 2)); // 1+2+3
        Assertions.assertEquals(9, st.rangeSum(1, 3)); // 2+3+4
    }

    @Test
    void testUpdateAndRangeSum() {
        SegmentTree st = new SegmentTree(new int[]{1, 2, 3, 4});
        st.update(0, 1000);
        Assertions.assertEquals(1005, st.rangeSum(0, 2)); // 1000+2+3
        st.update(2, 2000);
        Assertions.assertEquals(3002, st.rangeSum(0, 2)); // 1000+2+2000
        Assertions.assertEquals(2004, st.rangeSum(2, 3)); // 2000+4
    }

    @Test
    void testRangeSumFullArray() {
        SegmentTree st = new SegmentTree(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10});
        Assertions.assertEquals(55, st.rangeSum(0, 9)); // Sum of 1 to 10
        st.update(4, 100); // Updating element at index 4
        Assertions.assertEquals(150, st.rangeSum(0, 9)); // Sum with updated element
    }

    @Test
    void testEdgeCases() {
        SegmentTree st = new SegmentTree(new int[]{1, 2, 3, 4});
        Assertions.assertEquals(1, st.rangeSum(0, 0)); // Single element
        Assertions.assertEquals(4, st.rangeSum(3, 3)); // Single element at end
        st.update(3, 4000); // Update last element
        Assertions.assertEquals(4000, st.rangeSum(3, 3)); // Check updated value
        Assertions.assertEquals(4006, st.rangeSum(0, 3)); // Check sum including updated value
    }

    @Test
    void testLargeUpdates() {
        SegmentTree st = new SegmentTree(new int[]{1, 2, 3, 4});
        for (int i = 0; i < 4; i++) {
            st.update(i, i * 1000); // Updating elements with large values
        }
        Assertions.assertEquals(6000, st.rangeSum(0, 3)); // Sum of updated values 0+1000+2000+3000
        Assertions.assertEquals(3000, st.rangeSum(1, 2)); // Sum of middle values 1000+2000
    }

    @Test
    void testNonContiguousRanges() {
        SegmentTree st = new SegmentTree(new int[]{1, 2, 3, 4, 5, 6, 7, 8});
        Assertions.assertEquals(36, st.rangeSum(0, 7)); // Full sum 1+2+3+4+5+6+7+8
        st.update(0, 10); // Updating first element
        st.update(7, 20); // Updating last element
        Assertions.assertEquals(57, st.rangeSum(0, 7)); // Updated sum
        Assertions.assertEquals(27, st.rangeSum(1, 6)); // Middle sum without updated edges
    }

    @Test
    void testEmptySegmentTree() {
        SegmentTree st = new SegmentTree(new int[]{});
        Assertions.assertEquals(0, st.rangeSum(0, 0)); // Edge case of empty tree
    }

    @Test
    void testNegativeValues() {
        SegmentTree st = new SegmentTree(new int[]{-1, -2, -3, -4});
        Assertions.assertEquals(-6, st.rangeSum(0, 2)); // Sum of first three negative values
        st.update(1, -1000); // Updating to a larger negative value
        Assertions.assertEquals(-1004, st.rangeSum(0, 2)); // Updated sum
    }

    @Test
    void testUpdatesOnSameIndex() {
        SegmentTree st = new SegmentTree(new int[]{1, 2, 3, 4});
        st.update(2, 10);
        Assertions.assertEquals(17, st.rangeSum(0, 3)); // 1+2+10+4
        st.update(2, 20);
        Assertions.assertEquals(27, st.rangeSum(0, 3)); // 1+2+20+4
    }
}